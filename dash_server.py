import datetime
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly
import pandas as pd 
import plotly.express as px
import dash_bootstrap_components as dbc
from threading import Thread
from dash.dependencies import Input, Output
from dashboard.data_preprocessing import *
import base64
from brain_def import brain_3D


def main():

    #app = dash.Dash(external_stylesheets=[dbc.themes.DARKLY])
    app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
    app.css.config.serve_locally = False
    app.scripts.config.serve_locally = True

    ############################################################################################################################################################
    header = dbc.Nav(className="nav nav-pills", children=[
        ## about
        dbc.NavItem(html.Div([
            dbc.NavLink("About", href="/", id="about-popover", active=False),
            dbc.Popover(id="about", is_open=True, target="about-popover", children=[
                dbc.PopoverHeader("How it works")
            ])
        ])),
        ## links
        dbc.DropdownMenu(label="Links", nav=True, children=[
            dbc.DropdownMenuItem([html.I(className="fa fa-linkedin"), "  Contacts", ], target="_blank", href='https://www.linkedin.com/in/hamidi-djamel-eddine-389340153/'), 
            dbc.DropdownMenuItem([html.I(className="fa fa-gitlab"), "  Code"], target="_blank", href='https://gitlab.com/Djamel_Datascientist')
        ])
    ])
    ############################################################################################################################################################

    ############################################################################################################################################################
    navbar = dbc.Navbar(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src=app.get_asset_url('logo.png'), height="40px")),
                        dbc.Col(dbc.NavbarBrand("Centrum devloppement", className="ml-1", style={
                            'font-weight':'bold', })),
                    ],
                    align="center",
                    no_gutters=True,
                ),
                href="",
            ),
            dbc.NavbarToggler(id="navbar-toggler"),
            dbc.Collapse(header, id="navbar-collapse", navbar=True),

        ],
        color="dark",
        dark=True,
    )
    ############################################################################################################################################################

    ############################################################################################################################################################

    data_date = loadData('death')
    dropdownlist = html.Div([
        html.Div(
            [   
        dbc.Row(
            [
            html.H6("""Select the country""", 
                    style={'textAlign': 'right', 
                            'font-weight':'bold', 
                            'margin-left': '2em', 
                            'margin-right': '4em', 
                            'display': 'inline-block', 
                            'vertical-align' : 'middle', 
                            'width': '100%'})
            ], align="center"
            ),

        dcc.Dropdown(
            id='cities-dropdown',
            options=[{'label': i, 'value': i} for i in data_date["Country/Region"]],
            value='Algeria',style={ 'position' : 'middel',
                                    'font-weight' : '200',
                                    'textAlign': 'middel',
                                    'font-size' : '14px',
                                    'color' : 'black',
                                    'margin' : '1' ,
                                    'padding' : '8px',
                                    'display' : 'inline-block',
                                    'width' : '130px',
                                    'vertical-align' : 'middle', 
                                    'margin-left': '2em', 
                                    'margin-right': '2em',})

            ],
            className='row'
        )])

    ###########################################################################################################################################################
    @app.callback(
        Output('graphic_deaths', 'figure'),
        [Input('cities-dropdown', 'value')])
    def plot_death_data(value):
        map = selecdeathtData(value)
        return map

    @app.callback(
        Output('graphic_cases', 'figure'),
        [Input('cities-dropdown', 'value')])
    def plot_cases_data(value):
        map = selecdathcasestData(value)
        return map

    ############################################################################################################################################################
        
    Brain_3D = brain_3D()

    ############################################################################################################################################################
    predict_trace = plot_forecasr()
    tabs = dbc.Tabs([
    dbc.Tab(children=[html.Div(dcc.Graph(id='live-map', animate=False,),style={'height':500, 'width':'100%'})],
    label="Pandemic word map"
    ),
    dbc.Tab(children=[Brain_3D,
    ],label="Henesys"),
    dbc.Tab(children=[html.Div(
    html.Div([dropdownlist, dcc.Graph(id='graphic_cases',style={'height': 300}, animate=False,), dcc.Graph(id='graphic_deaths',style={'height': 300}, animate=False,)]))],
    label="Graphics data"
        ),
    dbc.Tab(children=[html.Div(

    html.Div([html.H3('Forecasting'), dcc.Graph(figure=predict_trace,style={'height': 300}, animate=False,)]))],

    label="Forecasting"
        )
    ])
    
    ############################################################################################################################################################

    @app.callback(Output('live-map', 'figure'),
                [Input('interval-component', 'n_intervals')])       
    def data_maping(n):
        token = open("mapbox_token").read()
        country_data = pd.read_csv('country_data.csv')
        fig = px.scatter_mapbox(country_data, lat="Latitude", lon="Longitude", size_max=180, hover_name="location", hover_data=['total_cases', 'total_deaths', 'total_tests']  ,
                        color_continuous_scale=px.colors.cyclical.IceFire, zoom=2, height=500)
        fig.update_layout( mapbox=dict(
        accesstoken=token,
        bearing=0,
        center=dict(
            lat=32,
            lon=-0
        ),
        pitch=0,
        zoom=2,
        style= "mapbox://styles/srepho/cjttho6dl0hl91fmzwyicqkzf"),)
        fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
        fig.layout.template = None
        return fig

    
    app.layout = dbc.Container(fluid=True,children=[html.Div(
        html.Div([
            navbar,
            html.Br(),
            tabs,
            dcc.Interval(
                id='interval-component',
                interval=1*1000*3600, # in milliseconds
                n_intervals=0
                ),
                html.Br(),
                html.H6("©Djamel Hamidi"),
                ])
            )
        ])

    ############################################################################################################################################################

    app.run_server()

    ############################################################################################################################################################

if __name__ == '__main__':
    tweb_app = Thread(target=main)
    data_loader= Thread(target=load_data)
    tweb_app.start()
    data_loader.start()
    tweb_app.join()
    data_loader.join()