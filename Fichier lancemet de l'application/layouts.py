# Setup
from result import Result
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from data_preprocessing import load_data, plot_prediction, plot_forecasr
from multiprocessing import Process
import pandas as pd
from threading import Thread
import plotly.express as px


class Layout():

    def header(self):
        header = dbc.Nav(className="nav nav-pills", children=[
        ## about
        dbc.NavItem(html.Div([
            dbc.NavLink("About", href="/", id="about-popover", active=False),
            dbc.Popover(id="about", is_open=False, target="about-popover", children=[
                dbc.PopoverHeader("How it works")
            ])
        ])),
        ## links
        dbc.DropdownMenu(label="Links", nav=True, children=[
            dbc.DropdownMenuItem([html.I(className="fa fa-linkedin"), "  Contacts"], target="_blank"), 
            dbc.DropdownMenuItem([html.I(className="fa fa-github"), "  Code"], target="_blank")
        ])
        ])
        return header

    def navebar(self):
        navbar = dbc.Navbar(
            [
                html.A(
                    # Use row and col to control vertical alignment of logo / brand
                    dbc.Row(
                        [
                            dbc.Col(html.Img(src=app.get_asset_url('favicon.png'), height="30px")),
                            dbc.Col(dbc.NavbarBrand("HEXATECH DASHBOARD", className="ml-1")),
                        ],
                        align="center",
                        no_gutters=True,
                    ),
                    href="http://hexatech-innovation.com/",
                ),
                dbc.NavbarToggler(id="navbar-toggler"),
                dbc.Collapse(header, id="navbar-collapse", navbar=True),

            ],
            color="dark",
            dark=True,
        )
        return navbar

    def mapfigure(self, map_figur):
        Map = dcc.Graph(figure=map_figur, id='Map')
        label="World map"
        structur = dbc.Row([
                ### input + panel
                dbc.Col(md=3, children=[
                    html.Br(),html.Br(),
                    html.Div(id="output-panel")
                ])
                ])
        return Map

    def Tabse(self):
        graphics = dbc.Tabs([
            dbc.Tab(dcc.Graph(id='live-graph', animate=True),
            dcc.Interval(
                id='graph-update',
                interval=360*1000*1, # in milliseconds
                n_intervals=0
            )),
            dbc.Tab(dcc.Graph(figure=prediction_fig, style={'height': 300},
            id='forecaste graphique'), label="Pandemic forecast")
            ])
        return graphics