# Setup
from result import Result
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from data_preprocessing import load_data, plot_prediction, plot_forecasr
from multiprocessing import Process
import pandas as pd
from threading import Thread
import plotly.express as px
from layouts import Layout

# Read data
def main():


    app = dash.Dash(__name__,external_stylesheets=[dbc.themes.DARKLY])
    @app.callback(Output('live-map', 'figure'),[Input('interval-component', 'n_intervals')])       
    def data_maping(n):
        token = open("mapbox_token").read()
        country_data = pd.read_csv('country_data.csv')
        fig = px.scatter_mapbox(country_data, lat="Latitude", lon="Longitude",size ='Mortality%',  hover_name="location", hover_data=['total_cases', 'total_deaths', 'total_tests']  ,
                        color_continuous_scale=px.colors.cyclical.IceFire, zoom=2, height=600)
        fig.update_layout(mapbox_style="dark",mapbox_accesstoken=token)
        fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
        fig.layout.template = None
        return fig
    
    map_figur = data_maping()
    prediction_fig = plot_forecasr()
    # App Instance
    # header bar
    header = dbc.Nav(className="nav nav-pills", children=[
        ## about
        dbc.NavItem(html.Div([
            dbc.NavLink("About", href="/", id="about-popover", active=False),
            dbc.Popover(id="about", is_open=False, target="about-popover", children=[
                dbc.PopoverHeader("How it works")
            ])
        ])),
        ## links
        dbc.DropdownMenu(label="Links", nav=True, children=[
            dbc.DropdownMenuItem([html.I(className="fa fa-linkedin"), "  Contacts"], target="_blank"), 
            dbc.DropdownMenuItem([html.I(className="fa fa-github"), "  Code"], target="_blank")
        ])
    ])
    navbar = dbc.Navbar(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src=app.get_asset_url('favicon.png'), height="30px")),
                        dbc.Col(dbc.NavbarBrand("HEXATECH DASHBOARD", className="ml-1")),
                    ],
                    align="center",
                    no_gutters=True,
                ),
                href="http://hexatech-innovation.com/",
            ),
            dbc.NavbarToggler(id="navbar-toggler"),
            dbc.Collapse(header, id="navbar-collapse", navbar=True),

        ],
        color="dark",
        dark=True,
    )

    #Map = dcc.Graph(figure=map_figur, id='Map')
    #label="World map"
    structur = dbc.Row([
            ### input + panel
            dbc.Col(md=3, children=[
                html.Br(),html.Br(),
                html.Div(id="output-panel")
            ])
            ])

    graphics = dbc.Tabs([
        dbc.Tab(dcc.Graph(id='live-graph', animate=True),
        ),
        dbc.Tab(dcc.Graph(figure=prediction_fig, style={'height': 300},
        id='forecaste graphique'), label="Pandemic forecast")
        ])
    # App Layout
    app.layout = dbc.Container(fluid=True, children=[
        ## Top
        html.Div([
            navbar,
            html.P('Dash to get covide 19 data on the word ©HAMIDI DJAMEL')]),
            structur,
            ### plots
            graphics
            ])


    app.run_server()



if __name__ == '__main__':
    tweb_app = Thread(target=main)
    data_loader= Thread(target=load_data)
    tweb_app.start()
    data_loader.start()
    tweb_app.join()
    data_loader.join()