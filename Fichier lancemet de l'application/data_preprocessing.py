import pandas as pd 
import numpy as np 
from geopy.geocoders import Nominatim
import numpy as np
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
import plotly.graph_objects as go
import time
import dash_core_components as dcc
import plotly.express as px
import dash_html_components as html
import dash_bootstrap_components as dbc
import matplotlib.pyplot as plt
import seaborn as sns
from dash.dependencies import Input, Output
from plotly.tools import mpl_to_plotly
import cufflinks as cf
from plotly.offline import init_notebook_mode, iplot
import plotly.io as pio
from dash.dependencies import Input, Output

def load_data():

    while True:
        try:
            Dataframe = pd.read_csv(
                'https://covid.ourworldindata.org/data/owid-covid-data.csv')

            SubDF = Dataframe.drop(['new_cases', 'new_deaths', 'total_cases_per_million', 'new_cases_per_million', 'total_deaths_per_million',
                                    'new_deaths_per_million', 'new_tests', 'total_tests_per_thousand', 'new_tests_per_thousand', 'tests_units'], axis=1)
            Struct_data_bydate = SubDF.groupby(SubDF.date).sum()
            Struct_data_bycountries = SubDF.groupby(SubDF.location).max()
            #Struct_data_bycountries.to_csv('country_data.csv')
            Struct_data_bycountries['Mortality%'] = (Struct_data_bycountries.total_deaths / Struct_data_bycountries.total_tests)*100
            Struct_data_bycountries["Mortality%"].fillna(0.01, inplace = True) 
            Struct_data_bycountries['Longitude'] = 0
            Struct_data_bycountries['Latitude'] = 0
            cnts = 0
            geolocator = Nominatim(user_agent="Convertion", scheme='http', timeout=None)

            for cnts in range(len(Struct_data_bycountries.index)):
                try:
                    loc = geolocator.geocode(Struct_data_bycountries.index[cnts])
                    Struct_data_bycountries.Longitude[cnts] = loc.longitude
                    Struct_data_bycountries.Latitude[cnts] = loc.latitude
                except:
                    pass
            
            Struct_data_bycountries.to_csv('country_data.csv')
            Struct_data_bydate.to_csv('date_data.csv')
        except:
            pass

        time.sleep(360*4)


def predict_model(x, y):   
    x_pred = np.arange(len(y)+20).reshape(-1, 1)
    
    #création de notre formul polynomial de 3 eme degrès , 
    #l'argument include_bias celon son activation permet de prendre en compte ou non le coeficient du x e puissance 0
    Regpolynome = PolynomialFeatures(degree=3, include_bias=False)
    
    #Calcule des coeficients de notre polynome
    x_ = Regpolynome.fit_transform(x.reshape(-1, 1))

    #Entrainement du modèle
    model = LinearRegression().fit(x_, y)

    #Calcul du coefficient de détermination (le R2) ou le score
    R2 = model.score(x_, y)

    
    #calcule de la prédiction 
    y_pred = model.predict(x_)

 
    Regpolynome.fit(x_pred)
    xx_ = Regpolynome.transform(x_pred)

    #Calcule les prédictions pour la France
    y_pred = model.predict(xx_)
    return y_pred, R2

def plot_prediction():

    DF_PREDICT = pd.read_csv('date_data.csv')
    def data(Y) : 
        Y = Y.to_numpy()
        X = np.arange(len(Y))
        return X, Y
    
    date, train_data = data(DF_PREDICT.total_cases)
    predict, accuracu = predict_model(date, train_data)
    fig = go.Figure()
    fig = go.Figure(go.Scatter(x=date, y=train_data, mode='lines',
                    name='lines'))
    fig.add_trace(go.Scatter(x=date, y=predict,
                    mode='lines+markers',
                    name='lines+markers'))
    fig.update_layout(
    autosize=False,
    height=600
    )   
    fig.layout.template = 'plotly_dark'
    fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

    return fig

def plot_forecasr():
    DF_PREDICT = pd.read_csv('date_data.csv')
    def data(Y) : 
        Y = Y.to_numpy()
        X = np.arange(len(Y))
        return X, Y
    
    date, train_data = data(DF_PREDICT.total_cases)
    predict, accuracu = predict_model(date, train_data)



    return dict(
        data=[
            dict(
                x=DF_PREDICT.index,
                y=train_data,
                name='Corona virus contamination',
                marker=dict(
                    color='rgb(220,20,60)'
                )
            ),             dict(

                y=predict,
                name='Prediction',
                marker=dict(
                    color='rgb(55, 83, 109)'
                )
            )
            ],
            layout=dict(
                title='Corona virus contamination forecast',
                showlegend=True,
                legend=dict(
                    x=0,
                    y=1.0
                ),
                margin=dict(l=40, r=0, t=40, b=30)
            )
        ) 



def Case_tabs(): 
    cases = html.Div(
            [
                html.Div(
                    [html.H6(id="well_text"), html.P("No. of Wells")],
                    id="wells",
                    className="mini_container",
                ),
                html.Div(
                    [html.H6("Total infected "), html.P("infected")],
                    className="mini_container",
                ),
                html.Div(
                    [html.H6("Total dathes"), html.P("Deaths")],
        
                    className="mini_container",
                ),
                html.Div(
                    [html.H6("Total tested"), html.P("tested")],
                    className="mini_container",
                ),
            ],
            id="info-container",
            className="row container-display",
        )
    return cases

  
def loadData(datainfo):
    data1 = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
    data2 = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"

    if datainfo == 'case' :
        data = pd.read_csv(data1) 

    else:
        data = pd.read_csv(data2) 

    data_date = data.drop(["Province/State","Lat","Long"], axis=1)
    return data_date
 
    
def processdata(countryname, data_date):
    data_country = data_date.groupby(data_date["Country/Region"]).max()
    data_country.head()
    return data_country.loc[countryname].to_frame().reset_index()

def plot_data(Countryname, countrydata, plot_name):
    countrydata = countrydata.set_index('index')
    figure = px.bar(countrydata, y=Countryname, x=countrydata.index, text=Countryname, template="plotly_dark",)
    figure.update_layout(
    title=plot_name,
    xaxis_title="Date",
    yaxis_title="Pandemic data",
    font=dict(
        family="Courier New, monospace",
        size=8,
        color="#7f7f7f"
    )
)

    return figure

def process_barplot_countrydata(Countryname, info, plot_name):
    data_date = loadData(info)
    country_data = processdata(Countryname, data_date)
    plot = plot_data(Countryname, country_data, plot_name)
    return plot

def selecdeathtData(country):
    plot = process_barplot_countrydata(country, 'death', 'Deaths data')
    return plot

def selecdathcasestData(country):
    plot = process_barplot_countrydata(country, 'case', 'Infected people data')
    return plot

