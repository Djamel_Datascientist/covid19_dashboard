import pandas as pd
from result import Result
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from data_preprocessing import load_data, plot_prediction, plot_forecasr
from multiprocessing import Process
import pandas as pd
from threading import Thread
import plotly.express as px
from layouts import Layout

baseURL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/"

def loadData(fileName, columnName):
    data = pd.read_csv(baseURL + fileName) \
             .drop(['Lat', 'Long'], axis=1) \
             .melt(id_vars=['Province/State', 'Country/Region'], 
                 var_name='date', value_name=columnName) \
             .astype({'date':'datetime64[ns]', columnName:'Int64'}, 
                 errors='ignore')
    data['Province/State'].fillna('<all>', inplace=True)
    data[columnName].fillna(0, inplace=True)
    return data

allData = loadData(
    "time_series_covid19_confirmed_global.csv", "CumConfirmed") \
  .merge(loadData(
    "time_series_covid19_deaths_global.csv", "CumDeaths")) \
  .merge(loadData(
    "time_series_covid19_recovered_global.csv", "CumRecovered"))

countries = allData['Country/Region'].unique()
countries.sort()

dbc.Container(fluid=True, children=[html.Div(html.H1('Case History of the Coronavirus (COVID-19)'),
html.Div(className="row", children=[
    html.Div(className="four columns", children=
        html.H5('Country'),
        dcc.Dropdown(id='country',
        options=[{'label':c, 'value':c} \
        for c in countries],
            value='Italy'
        )
    ),
    html.Div(className="four columns", children=[
        html.H5('State / Province'),
        dcc.Dropdown(
            id='state'
        )
    ]),
    html.Div(className="four columns", children=[
        html.H5('Selected Metrics'),
        dcc.Checklist(
            id='metrics',
            options=[{'label':m, 'value':m} for m in \
                ['Confirmed', 'Deaths', 'Recovered']],
            value=['Confirmed', 'Deaths']
                )
            ])
        ]),
    )])

