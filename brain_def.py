import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_colorscales as dcs
import numpy as  np
import json
from textwrap import dedent as d
import plotly.graph_objs as go

def brain_3D():
    DEFAULT_COLORSCALE = [[0, 'rgb(51, 169, 255)'], [0.25, 'rgb(93, 109, 126)'],\
            [0.5, 'rgb(51, 169, 255)'], [0.75, 'rgb(238, 91, 39)'], \
            [1, 'rgb(88, 214, 141)']]

    DEFAULT_COLORSCALE_NO_INDEX = [ea[1] for ea in DEFAULT_COLORSCALE]

    def read_mniobj(file):
        ''' function to read a MNI obj file '''

        def triangulate_polygons(list_vertex_indices):
            ''' triangulate a list of n indices  n=len(list_vertex_indices) '''

            for k in range(0, len(list_vertex_indices), 3):
                    yield list_vertex_indices[k: k+3]

        fp=open(file,'r')
        n_vert=[]
        n_poly=[]
        k=0
        list_indices=[]
        # Find number of vertices and number of polygons, stored in .obj file.
        # Then extract list of all vertices in polygons
        for i, line in enumerate(fp):
            if i==0:
            #Number of vertices
                n_vert=int(line.split()[6])
                vertices=np.zeros([n_vert,3])
            elif i<=n_vert:
                vertices[i-1]=list(map(float, line.split()))
            elif i>2*n_vert+5:
                if not line.strip():
                    k=1
                elif k==1:
                    list_indices.extend(line.split())
        #at this point list_indices is a list of strings, and each string is a vertex index, like this '23'
        #maps in Python 3.6 returns a generator, hence we convert it to a list
        list_indices=list(map(int, list_indices))#conver the list of string indices to int indices
        faces=np.array(list(triangulate_polygons(np.array(list_indices))))
        return vertices, faces

    def standard_intensity(x,y,z):
        ''' color the mesh with a colorscale according to the values
                                of the vertices z-coordinates '''
        return z

    def plotly_triangular_mesh(vertices, faces, intensities=None, colorscale="Viridis",
                            flatshading=False, showscale=False, reversescale=False, plot_edges=False):
        ''' vertices = a numpy array of shape (n_vertices, 3)
            faces = a numpy array of shape (n_faces, 3)
            intensities can be either a function of (x,y,z) or a list of values '''

        x,y,z=vertices.T
        I,J,K=faces.T

        if intensities is None:
            intensities = standard_intensity(x,y,z)

        if hasattr(intensities, '__call__'):
            intensity=intensities(x,y,z)#the intensities are computed  via a function,
                                        #that returns the list of vertices intensities
        elif  isinstance(intensities, (list, np.ndarray)):
            intensity=intensities#intensities are given in a list
        else:
            raise ValueError("intensities can be either a function or a list, np.array")

        mesh=dict(
            type='mesh3d',
            x=x, y=y, z=z,
            colorscale=colorscale,
            intensity= intensities,
            flatshading=flatshading,
            i=I, j=J, k=K,
            name='',
            showscale=showscale
        )

        mesh.update(lighting=dict( ambient= 0.18,
                                    diffuse= 1,
                                    fresnel=  0.1,
                                    specular= 1,
                                    roughness= 0.1,
                                    facenormalsepsilon=1e-6,
                                    vertexnormalsepsilon= 1e-12))

        mesh.update(lightposition=dict(x=100,
                                        y=200,
                                        z= 0))

        if  showscale is True:
                mesh.update(colorbar=dict(thickness=20, ticklen=4, len=0.75))

        if plot_edges is False: # the triangle sides are not plotted
            return  [mesh]
        else:#plot edges
            #define the lists Xe, Ye, Ze, of x, y, resp z coordinates of edge end points for each triangle
            #None separates data corresponding to two consecutive triangles
            tri_vertices= vertices[faces]
            Xe=[]
            Ye=[]
            Ze=[]
            for T in tri_vertices:
                Xe+=[T[k%3][0] for k in range(4)]+[ None]
                Ye+=[T[k%3][1] for k in range(4)]+[ None]
                Ze+=[T[k%3][2] for k in range(4)]+[ None]
            #define the lines to be plotted
            lines=dict(type='scatter3d',
                    x=Xe,
                    y=Ye,
                    z=Ze,
                    mode='lines',
                    name='',
                    line=dict(color= 'rgb(70,70,70)', width=1)
                )
            return [mesh, lines]

    pts, tri=read_mniobj("surf_reg_model_both.obj")
    intensities=np.loadtxt('aal_atlas.txt')
    data=plotly_triangular_mesh(pts, tri, intensities,
                                colorscale=DEFAULT_COLORSCALE, flatshading=False,
                                showscale=False, reversescale=False, plot_edges=False)
    data[0]['name'] = 'human_atlas'

    plot_layout = dict(
            title = '',
            margin=dict(t=0,b=0,l=0,r=0),
            font=dict(size=12, color='white'),
            showlegend=False,
            bgcolor="transparent",
            paper_bgcolor = 'rgba(0,0,0,0)',
			plot_bgcolor = 'rgba(0,0,0,0)',              
            )

    styles = {
        'position' : 'middel',
        'margin': 'auto',
        'height': '600px',
        'width': '100%',
        'background-image': 'url("assets/back_G.jpg")',
        'background-repeat': 'no-repeat',

    }

    '''
    ~~~~~~~~~~~~~~~~
    ~~ APP LAYOUT ~~
    ~~~~~~~~~~~~~~~~a
    '''

    app_layout = dcc.Graph(
            id='brain-graph',
            figure={
                'data': data,
                'layout': plot_layout,
            },
            config={'editable': True, 'scrollZoom': False},
            style=styles
        )


    return app_layout
